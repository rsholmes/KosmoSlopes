# KosmoSlopes
 Kosmo Version of the [Kassutronics Slope](https://kassu2000.blogspot.com/2020/10/slope.html) Module

Forked from [https://github.com/BenRufenacht/KosmoSlopes](https://github.com/BenRufenacht/KosmoSlopes); changes:

* Changed graphics and slightly altered layout of front panel
* Revised EOR and EOC outputs
* Replaced beads with diodes
* Changed bypass caps and C2 to ceramic
* Changed Zener footprints to DO-35
* Some routing revisions
* Moved jacks board into main project
* Added labels to boards
* Directory structure reorganized

## License:

[CC-BY-SA](https://creativecommons.org/licenses/by-sa/3.0/)

## Current draw
38 mA +12 V, 24 mA -12 V


## Photos

![](Images/slope.jpg)

## Documentation

* [Schematic](Docs/slope_schematic.pdf)
* PCB layout: 
    * Main PCB: [front](Docs/Layout/slope_FrontPCB/slope_FrontPCB_F.SilkS.pdf), [back](Docs/Layout/slope_FrontPCB/slope_FrontPCB_B.SilkS.pdf)
    * Jacks PCB: [front](Docs/Layout/slope_BackPCB/slope_BackPCB_F.SilkS.pdf), [back](Docs/Layout/slope_BackPCB/slope_BackPCB_B.SilkS.pdf)
* [BOM](Docs/BOM/slope_bom.md)
* [Blog post](https://www.richholmes.xyz/analogoutput/2023-10-30_display-and-slope/)

## Git repository

* [https://gitlab.com/rsholmes/KosmoSlopes](https://gitlab.com/rsholmes/KosmoSlopes)

