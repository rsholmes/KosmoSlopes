# slope.kicad_sch BOM

Sun 17 Dec 2023 08:53:30 AM EST

Generated from schematic by Eeschema 7.0.9-7.0.9~ubuntu22.04.1

**Component Count:** 119

| Refs | Qty | Component | Description | Manufacturer | Vendor | SKU | Note |
| ----- | --- | ---- | ----------- | ---- | ---- | ---- | ---- |
| C1 | 1 | 10nF | Polyester film capacitor, 2.5 mm pitch |  | Tayda |  |  |
| C2 | 1 | 100pF | Ceramic disc capacitor, 2.5 mm pitch |  | Tayda |  |  |
| C4, C5 | 2 | 10uF | Electrolytic capacitor, 2.5 mm pitch |  | Tayda |  |  |
| C6–8, C10–17 | 11 | 100nF | Ceramic disc capacitor, 2.5 mm pitch |  | Tayda |  |  |
| D1–3, D5, D6, D9, D11 | 7 | 1N4148 | Standard switching diode, DO-35 |  | Tayda | A-157 |  |
| D4, D16, D17 | 3 | 1N5817 | Schottky Barrier Rectifier Diode, DO-41 |  | Tayda | A-159 |  |
| D7, D8, D10, D14 | 4 | BZX79-C7V5 | Zener, 7.5 V | Nexperia | Digi-Key | 1727-4683-1-ND | or 1N5236B ? |
| D12, D13 | 2 | LED_green | Light emitting diode |  | Tayda | A-1553 |  |
| D15 | 1 | LED_Dual_Bidirectional | Dual LED, bidirectional |  |  |  |  |
| J1 | 1 | Synth_power_2x5 | Pin header 2.54 mm 2x5 |  | Tayda | A-2939 |  |
| J2 | 1 | Conn_01x12 | 1x12 pin socket 2.54 mm pitch |  |  |  |  |
| J3–6, J8–10 | 7 | AudioJack2 | Audio Jack, 2 Poles (Mono / TS) |  | Tayda | A-1121 |  |
| J7 | 1 | Conn_01x12 | 1x12 pin header 2.54 mm pitch |  |  |  |  |
| J11–15 | 5 | DIP-8 | 8 pin DIP socket |  |  |  |  |
| J16 | 1 | DIP-14 | 14 pin DIP socket |  | Tayda | A-004 |  |
| Q1, Q2 | 2 | 2N3906 | -0.2A Ic, -40V Vce, Small Signal PNP Transistor, TO-92 |  | Tayda | A-117 |  |
| Q3, Q4 | 2 | 2N3904 | Small Signal NPN Transistor, TO-92 |  | Tayda | A-111 |  |
| R1, R8, R10, R12, R17–19, R25, R27, R28, R31, R32, R39–41 | 15 | 100k | Resistor |  | Tayda |  |  |
| R2, R15, R22–24, R35, R36, R38, R43, R44 | 10 | 1k | Resistor |  | Tayda |  |  |
| R3–5, R9, R13, R16 | 6 | 10k | Resistor |  | Tayda |  |  |
| R6, R11, R14 | 3 | 1M | Resistor |  | Tayda |  |  |
| R7 | 1 | 2.2M | Resistor |  | Tayda |  |  |
| R20, R21, R33, R34 | 4 | 47k | Resistor |  | Tayda |  |  |
| R26 | 1 | LED-BI | Resistor |  | Tayda |  | 1k or choose for desired brightness |
| R29, R30 | 2 | 22k | Resistor |  | Tayda |  |  |
| R37, R42 | 2 | LED | Resistor |  | Tayda |  | 2k or choose for desired brightness |
| R45, R46 | 2 | 750R | Resistor |  | Tayda |  |  |
| RV1, RV2, RV5 | 3 | 10k | Potentiometer |  | Tayda |  |  |
| RV3, RV4, RV6, RV7 | 4 | 100k | Potentiometer |  | Tayda |  |  |
| SW1 | 1 | SW_SPDT | SPDT switch ON-ON |  | Tayda | A-3186 |  |
| U1, U2, U5, U6 | 4 | TL072 | Dual operational amplifier, DIP-8 |  | Tayda | A-037 |  |
| U3 | 1 | TL074 | Quad operational amplifier, DIP-14 |  | Tayda | A-1138 |  |
| U4 | 1 | TLC3702 | Low-Power, Low-Offset Voltage, Dual Comparators, DIP-8/SOIC-8/TO-99-8 |  | Digi-Key | 296-1841-5-ND |  |
| ZKN1–3 | 3 | Knob_MF-A01 | MF-A01 knob |  |  |  |  |
| ZKN4–7 | 4 | Knob_MF-A03 | MF-A03 knob |  |  |  |  |

## Resistors in color band order
|Value|Qty|Refs|
|----|----|----|
|1k|10|R2, R15, R22–24, R35, R36, R38, R43, R44|
|10k|6|R3–5, R9, R13, R16|
|100k|15|R1, R8, R10, R12, R17–19, R25, R27, R28, R31, R32, R39–41|
|1M|3|R6, R11, R14|
|22k|2|R29, R30|
|2.2M|1|R7|
|47k|4|R20, R21, R33, R34|
|750R|2|R45, R46|
|LED-BI|1|R26|
|LED|2|R37, R42|

